import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlumneTest {

    Alumne alumne;

    @BeforeEach
    void init(){
        alumne = new Alumne("Marc Vives");
    }
    @Test
    void testNom(){
        assertEquals("Marc Vives", alumne.getNom());
    }

    @Test
    void testInicialmentSuspes(){
        assertFalse(alumne.isaAprovat());
    }

    @Test
    void testAprovatSiEstudia(){
        alumne.estudiar();
        assertTrue(alumne.isaAprovat());
    }
}