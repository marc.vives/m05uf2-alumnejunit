# Activitat

Basant-nos amb l'exemple <https://www.youtube.com/watch?v=Bld3644bIAo&feature=emb_title>:

Crear una Classe **Alumne** amb els atributs i métode:

- aprovat:boolean
- nom:String
- estudiar()

Genera la Classe **AlumneTest** amb els test:
- testNom()
- testInicialmentSuspes()
- testAprovatSiEstudia()
