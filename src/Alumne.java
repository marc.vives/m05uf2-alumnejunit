public class Alumne {

    private String nom;
    private boolean aprovat=false;

    public Alumne(String nom) {
        this.nom=nom;
    }


    public void estudiar(){
        aprovat = true;
    }

    public String getNom() {
        return nom;
    }

    public boolean isaAprovat() {
        return aprovat;
    }
}
